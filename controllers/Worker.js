const {check, validationResult} = require("express-validator")

let db = require("../models/index.js");
const ErroeHandler = require("../utils/ErrorHandler")

module.exports.Get = async (req, res) => {
  try{
    const errors = validationResult(req)
    if(!errors.isEmpty()){
      return res.status(200).json({
        errors: errors.array(),
        message: "Некорректные данные"
      })
    }
    
    let Response = {}
    await db.sequelize.transaction(async  transaction => {
      await db.sequelize.query('CALL `selectWorker`();',
        {
          type: db.sequelize.QueryTypes.SELECT,
          transaction: transaction
        }
      ).then(result => {
        console.log(result)
        Response.Data = result[0]
      })
    })
    
    Response.message = "Ok"
    return res.status(200).json(Response)
  }
  catch (e) {
    ErroeHandler(res, e)
  }
}

module.exports.Search = async (req, res) => {
  try{
    const errors = validationResult(req)
    if(!errors.isEmpty()){
      return res.status(200).json({
        errors: errors.array(),
        message: "Некорректные данные"
      })
    }
    
    let Response = {}
    
    await db.sequelize.transaction(async  transaction => {
      await db.sequelize.query('CALL `searchWorker` (?)', {replacements: [req.body.Data.Id]},
        {
          type: db.sequelize.QueryTypes.SELECT,
          transaction: transaction
        }
      ).then(result => {
        console.log(result)
        Response.Data = result
      })
    })
    Response.message = "Ok"
    return res.status(200).json(Response)
  }
  catch (e) {
    ErroeHandler(res, e)
  }
}

module.exports.Insert = async (req, res) => {
  try{
    const errors = validationResult(req)
    if(!errors.isEmpty()){
      return res.status(200).json({
        errors: errors.array(),
        message: "Некорректные данные"
      })
    }
    
    const {fullName, phone,password,balance,idLevel,preferences,cardNumber,namePosition,percent,telegram,dateBegin,workingShift} = req.body.Data
    
    let Response = {}
    await db.sequelize.transaction(async  transaction => {
      await db.sequelize.query('CALL `insertWorker` (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        {replacements: [fullName, phone,password,balance,idLevel,preferences,cardNumber,namePosition,percent,telegram, "2021-05-11 01:40:00.000000"]},
        {
          type: db.sequelize.QueryTypes.INSERT,
          transaction: transaction
        }
      ).then(result => {
        console.log(result)
        Response.message = "Запись добавлена"
      })
      let q = "";
      await db.sequelize.query('SELECT * FROM worker ORDER BY ID DESC LIMIT 1',
        {replacements: []},
        {
          type: db.sequelize.QueryTypes.INSERT,
          transaction: transaction
        }
      ).then(result => {
        console.log("asd")
        console.log(result[0][0])
        
          workingShift.forEach(el => {
            if(el.Flag){
              q+= 'CALL `insertCompositionShift` ('+el.id+', '+result[0][0].id+');';
            }
          })
      })

      await db.sequelize.query(q,
        {
            type: db.sequelize.QueryTypes.INSERT,
            transaction: transaction
            }
          ).then(result1 => {
            console.log(result1)
        })
   }) 
    
    return res.status(200).json(Response)
  } 
  catch (e) {
    ErroeHandler(res, e)
  }
}

module.exports.Update = async (req, res) => {
  try{
    const errors = validationResult(req)
    if(!errors.isEmpty()){
      return res.status(200).json({
        errors: errors.array(),
        message: "Некорректные данные"
      })
    }
    
    const {Id, fullName, phone,password,balance,idLevel,preferences,cardNumber,namePosition,percent,telegram,dateBegin,workingShift} = req.body.Data
    
    let Response = {}

    await db.sequelize.transaction(async  transaction => {
      await db.sequelize.query('CALL `updateWorker` (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        {replacements: [fullName, phone,password,balance,idLevel,preferences,cardNumber,namePosition,percent,telegram, "2021-05-11 01:40:00.000000", Id]},
        {
          type: db.sequelize.QueryTypes.INSERT,
          transaction: transaction
        }
      ).then(result => {
        console.log(result)
        Response.message = "Запись изменена"
      })
      let q = "";
      workingShift.forEach(el => {
            if(el.Flag){
              q+= 'CALL `insertCompositionShift` ('+el.id+', '+Id+');';
            }
          })
      await db.sequelize.query(q,
        {
            type: db.sequelize.QueryTypes.INSERT,
            transaction: transaction
            }
          ).then(result1 => {
            console.log(result1)
        })
   }) 
    
    return res.status(200).json(Response)
  } 
  catch (e) {
    ErroeHandler(res, e)
  }
}

module.exports.Delete = async (req, res) => {
  try{
    const errors = validationResult(req)
    if(!errors.isEmpty()){
      return res.status(200).json({
        errors: errors.array(),
        message: "Некорректные данные"
      })
    }
    
    let Response = {}
    
    await db.sequelize.transaction(async  transaction => {
      await db.sequelize.query('CALL `deleteWorker` (?)', {replacements: [req.body.Data.Id]},
      {
        type: db.sequelize.QueryTypes.INSERT,
        transaction: transaction
      }
      ).then(result => {
        console.log(result)
        Response.message = "Обьект удален"
        Response.Data = result
      })
    })
    
    return res.status(200).json(Response)
  } 
  catch (e) {
    ErroeHandler(res, e)
  }
}