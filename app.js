const express = require("express")
const config = require("config")
const app = new express()

const PORT = config.get("port") || 5000

app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.use('/api/Address', require('./routes/Address'))
app.use('/api/CompositionShift', require('./routes/CompositionShift'))
app.use('/api/Expenses', require('./routes/Expenses'))
app.use('/api/Level', require('./routes/Level'))
app.use('/api/Pay', require('./routes/Pay'))
app.use('/api/Penalty', require('./routes/Penalty'))
app.use('/api/Room', require('./routes/Room'))
app.use('/api/Rules', require('./routes/Rules'))
app.use('/api/Worker', require('./routes/Worker'))
app.use('/api/WorkingShift', require('./routes/WorkingShift'))

async function start() {
  try{
    app.listen(PORT, () => console.log(`Starn in port ${PORT}`))
  } catch (e) {
    console.log('Server Error', e.message)
    process.exit(1)
  }
}

start()