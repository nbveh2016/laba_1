import React from 'react';
import * as axios from 'axios';
import s from './CRUD.module.css';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import {NavLink, Route, withRouter} from 'react-router-dom';

import {createPdf} from './PDFDoc.js';

const request = (Obj, Metod, Data) => {
	return (
        axios.post("/api" + "/" + Obj + "/" + Metod, {
            Data: Data
        })
        .then((response) => {
          if(response.data.message) alert(response.data.message)
          return response.data
        })
        .catch(error => console.log(error))
    );
}

var tableToExcel = (csvData, fileName) => {
    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExtension = '.xlsx';
    
    const ws = XLSX.utils.json_to_sheet(csvData);
    const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
    const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    const data = new Blob([excelBuffer], {type: fileType});
    FileSaver.saveAs(data, fileName + fileExtension);
};

class Select extends React.Component {
    constructor(props){
        super(props);

        let custs = []
        for (let i = 0; i <= 25; i++) {
          custs.push({firstName: `first${i}`, lastName: `last${i}`,
          email: `abc${i}@gmail.com`, address: `000${i} street city, ST`, zipcode: `0000${i}`});
        }

        this.state = {
            Data: null,
            Custs: custs
        }
    }
    componentDidMount(){
        request(this.props.Table, "SELECT")
        .then((data) => {
            this.setState({
                Data: Object.values(data.Data).length? Object.values(data.Data).map((el,ind) => {
                    return el;
                }): null
            });
        });
    }
    
	render(){
        console.log(this.state)
    return (
    <div>
        {this.state.Option == "UPDATE" && this.state.SId && 
            <div>
                <button
                    onClick={()=>{
                        this.setState({
                            Id: null,
                            Option: null
                        })
                    }}
                >Назад
                </button>
                <Updata Table={this.props.Table} Form={this.props.Form} SId={this.state.SId}/>
            </div>
        }
        {this.state.Option == "DELETE" && this.state.SId && 
            <div>
                <button
                    onClick={()=>{
                        this.setState({
                            Id: null,
                            Option: null
                        })
                    }}
                >Назад
                </button>
                <Delete Table={this.props.Table} Form={this.props.Form} SId={this.state.SId}/>
            </div>
        }
        {!this.state.Option && !this.state.Id &&
            <div className={"tableOverlay"}>
            <table  className={"table table-bordered table-striped"}>
                        <thead className={"thead-dark"}>
                            <tr key={"key_tr"}>
                                {this.state.Data && Object.keys(this.state.Data[0]).map((el, ind) => {
                                    return (<th key={ind}>{el}</th>)
                                })}
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.Data && this.state.Data.map((el, ind) => {
                                return (
                                    <tr key={ind}>
                                        {Object.values(el).map((el1, ind1) => {
                                            if(ind1 === 0){
                                                return (
                                                    <td>
                                                        <button
                                                            onClick={() => {
                                                                this.setState({
                                                                    SId: el.id,
                                                                    Option: "UPDATE"
                                                                })
                                                            }}
                                                        >UPDATE</button>
                                                        <button
                                                            onClick={() => {
                                                                this.setState({
                                                                    SId: el.id,
                                                                    Option: "DELETE"
                                                                })
                                                            }}
                                                        >DELETE</button>
                                                    </td>
                                                )
                                            }else{
                                                return (
                                                    <td key={ind1}>{el1}</td>
                                                )
                                            }
                                        })}
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
            </div>
        }
    </div>
   )
  }
}
class Search extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            Data: null,
            value: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({
            value: event.target.value
        });
    }
    handleSubmit() {
        request(this.props.Table, "SEARCH", {Id: this.state.value})
        .then((data) => {
            this.setState({
                Data: Object.values(data.Data).length? Object.values(data.Data).map((el,ind) => {
                    delete el.id;
                    return el;
                }): null
            });
        });
    }
    
	render(){
        return (
            <div>
                <div>
                    <input type="text" value={this.state.value} onChange={this.handleChange} />
                    <button onClick={this.handleSubmit}>Search</button>
                </div>
                <table className={"table"}>
                    <thead>
                        <tr key={"key_tr"}>
                            {this.state.Data && Object.keys(this.state.Data[0]).map((el, ind) => {
                                return (<th key={ind}>{el}</th>)
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.Data && this.state.Data.map((el, ind) => {
                            return (
                                <tr key={ind}>
                                    {Object.values(el).map((el1, ind1) => {
                                        return (
                                            <td key={ind1}>{el1}</td>
                                        )
                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
	   )
    }
}
class Insert extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            Data: null,
            asd: 0
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        
        this.props.Form.forEach(el => {
            this.state = {...this.state, [el.Name + "Current"]: "", [el.Name + "Select"]: null};
        });
    }
    
    componentDidMount(){
        this.props.Form.forEach(el => {
            if(el.Select){
                request(el.Select, "SELECT")
                .then((data) => {
                    if (el.Join) {
                        this.setState({
                            [el.Name + "Current"]: Object.values(data.Data).length? Object.values(data.Data).map(el => {
                                return {...el, Flag: false}
                            }): null
                        });
                    }else{
                        this.setState({
                            [el.Name + "Current"]: Object.values(data.Data).length? Object.values(data.Data)[0]["id"]: null,
                            [el.Name + "Select"]: Object.values(data.Data).length? Object.values(data.Data).map(el => {
                                return {...el, ok: false}
                            }): null
                        });
                    }
                });
            }
        });
    }
    
    handleSubmit(event) {
        console.log(this.state);
        let Data = {};
        this.props.Form.forEach(el => {
            Data = {...Data, [el.Name]: this.state[el.Name + "Current"]};
        });
        request(this.props.Table, "INSERT", Data);
        event.preventDefault();
    }
    
	render(){
        console.log(this.state)
        return (
            <div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        {
                        this.props.Form.map((el, ind) => {
                            if(el.Select && el.Join){
                                return(
                                    <div>
                                        {el.Name}
                                        {this.state[el.Name + "Current"] && (this.state[el.Name + "Current"]).map((el1, ind1) => {
                                            
                                            return (
                                                <div>
                                                    
                                                    <div 
                                                        key={ind1}
                                                        onClick={()=>{
                                                            let temp = this.state[el.Name + "Current"];
                                                            temp[ind1].Flag = !temp[ind1].Flag;
                                                            console.log(temp);
                                                            this.setState({
                                                                [el.Name + "Current"]: temp,
                                                                asd: this.state.asd++
                                                            })
                                                        }}
                                                        >{el1.name? el1.name: el1.id}{el1.Flag? " да": " нет" }</div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                )
                            }

                            if(el.Select && !el.Join){
                                return (
                                    <div>
                                        {el.Name}
                                        <select key={ind} onChange={(event) => this.setState({[el.Name + "Current"]: event.target.value})}>
                                            {this.state[el.Name + "Select"] && (this.state[el.Name + "Select"]).map((el1, ind1) => {
                                               return (
                                                   <option key={ind1} value={el1.id}>{el1.name? el1.name: el1.id}</option>
                                               )
                                            })}
                                        </select>
                                    </div>
                                )
                            }
                            if(!el.Select && !el.Join){
                                return(
                                    <div>
                                        {el.Name}
                                        <input key={ind} value={this.state[el.Name + "Current"]} 
                                            onChange={(event) => this.setState({[el.Name + "Current"]: event.target.value})}
                                            required
                                        />
                                    </div>
                                )
                            }
                            
                            })
                        }
                    
                        <input type="submit" value="Insert" />
                    </form>
                    
                </div>
            </div>
	   )
    }
}
class Updata extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            Data: null,
            asd: 0
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        
        this.props.Form.forEach(el => {
            this.state = {...this.state, [el.Name + "Current"]: "", [el.Name + "Select"]: null};
        });
    }
    
    componentDidMount(){
        this.props.Form.forEach(el => {
            if(el.Select){
                request(el.Select, "SELECT")
                .then((data) => {
                    if (el.Join) {
                        this.setState({
                            [el.Name + "Current"]: Object.values(data.Data).length? Object.values(data.Data).map(el => {
                                return {...el, Flag: false}
                            }): null
                        });
                    }else{
                        this.setState({
                            [el.Name + "Current"]: Object.values(data.Data).length? Object.values(data.Data)[0]["id"]: null,
                            [el.Name + "Select"]: Object.values(data.Data).length? Object.values(data.Data).map(el => {
                                return {...el, ok: false}
                            }): null
                        });
                    }
                });
            }
        });
    }
    
    handleSubmit(event) {
        console.log(this.state);
        let Data = {Id: this.props.SId};
        this.props.Form.forEach(el => {
            Data = {...Data, [el.Name]: this.state[el.Name + "Current"]};
        });
        request(this.props.Table, "UPDATE", Data);
        event.preventDefault();
    }
    
	render(){
        return (
            <div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        {
                        this.props.Form.map((el, ind) => {
                            if(el.Select && el.Join){
                                return(
                                    <div>
                                        {el.Name}
                                        {this.state[el.Name + "Current"] && (this.state[el.Name + "Current"]).map((el1, ind1) => {
                                            
                                            return (
                                                <div>
                                                    
                                                    <div 
                                                        key={ind1}
                                                        onClick={()=>{
                                                            let temp = this.state[el.Name + "Current"];
                                                            temp[ind1].Flag = !temp[ind1].Flag;
                                                            console.log(temp);
                                                            this.setState({
                                                                [el.Name + "Current"]: temp,
                                                                asd: this.state.asd++
                                                            })
                                                        }}
                                                        >{el1.name? el1.name: el1.id}{el1.Flag? " да": " нет" }</div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                )
                            }

                            if(el.Select && !el.Join){
                                return (
                                    <div>
                                        {el.Name}
                                        <select key={ind} onChange={(event) => this.setState({[el.Name + "Current"]: event.target.value})}>
                                            {this.state[el.Name + "Select"] && (this.state[el.Name + "Select"]).map((el1, ind1) => {
                                               return (
                                                   <option key={ind1} value={el1.id}>{el1.name? el1.name: el1.id}</option>
                                               )
                                            })}
                                        </select>
                                    </div>
                                )
                            }
                            if(!el.Select && !el.Join){
                                return(
                                    <div>
                                        {el.Name}
                                        <input key={ind} value={this.state[el.Name + "Current"]} 
                                            onChange={(event) => this.setState({[el.Name + "Current"]: event.target.value})}
                                            required
                                        />
                                    </div>
                                )
                            }
                            
                            })
                        }
                        <input type="submit" value="Updata" />
                    </form>
                    
                </div>
            </div>
	   )
    }
}
class Delete extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            Data: null,
            value: "1"
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({
            value: event.target.value
        });
    }
    handleSubmit() {
        request(this.props.Table, "DELETE", {Id: this.props.SId});
    }
    
	render(){
        return (
            <div>
                <div>
                    <button onClick={this.handleSubmit}>Delete</button>
                </div>
            </div>
	   )
    }
}

class Tools extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            CurrentTool: "SELECT"
        }
    }
    
	render(){
        return (
            <div class="downPanel">
                <div class="downLink">
                    <NavLink to={"/" + this.props.Table + "/SELECT"} onClick={()=> this.setState({CurrentTool: "SELECT"})}>SELECT </NavLink>
                    <NavLink to={"/" + this.props.Table + "/SEARCH"} onClick={()=> this.setState({CurrentTool: "SEARCH"})}>SEARCH </NavLink>
                    <NavLink to={"/" + this.props.Table + "/INSERT"} onClick={()=> this.setState({CurrentTool: "INSERT"})}>INSERT </NavLink>
                </div>
                    <Route path={"/" + this.props.Table + "/SELECT"}>
                        <Select Table={this.props.Table} Form={this.props.Form}/>
                    </Route>
                    <Route path={"/" + this.props.Table + "/SEARCH"}>
                        <Search Table={this.props.Table} Form={this.props.Form}/>
                    </Route>
                    <Route path={"/" + this.props.Table + "/INSERT"}>
                        <Insert Table={this.props.Table} Form={this.props.Form}/>
                    </Route>
            </div>
	   )
    }
}



export default class CRUD extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            Table: "Address",
            Form: [{Name: "Name", Select: null}]
        }
    }
    
	render(){
        return (
            <div className={"structure"}>
            <div className={"leftPanel"}>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Address",
                            Form: [{Name: "Name", Select: null}]
                        })}
                        to={"/Address"}>Address</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "CompositionShift",
                            Form: [
                                {Name: "idShift", Select: null},
                                {Name: "idWorker", Select: null}
                            ]
                        })}
                        to={"/CompositionShift"}>CompositionShift</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Expenses",
                            Form: [
                                {Name: "Name", Select: null},
                                {Name: "Pdate", Select: null},
                                {Name: "Sum", Select: null}
                            ]
                        })}
                        to={"/Expenses"}>Expenses</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Level",
                            Form: [
                                {Name: "Name", Select: null},
                                {Name: "Criteria", Select: null},
                                {Name: "Bonus", Select: null}
                                ]
                        })}
                        to={"/Level"}>Level</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Pay",
                            Form: [
                                {Name: "idWorker", Select: "Worker"},
                                {Name: "Pdate", Select: null},
                                {Name: "Sum", Select: null}
                            ]
                        })}
                        to={"/Pay"}>Pay</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Penalty",
                            Form: [
                                {Name: "idWorker", Select: "Worker"},
                                {Name: "Name", Select: null},
                                {Name: "Sum", Select: null}
                            ]
                        })}
                        to={"/Penalty"}>Penalty</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Room",
                            Form: [
                                {Name: "IdAddress", Select: "Address"}, 
                                {Name: "Num", Select: null}
                            ]
                        })}
                        to={"/Room"}>Room</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Rules",
                            Form: [
                                {Name: "Head", Select: null}, 
                                {Name: "Text", Select: null}
                            ]
                        })}
                        to={"/Rules"}>Rules</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "Worker",
                            Form: [
                                {Name: "fullName", Select: null},
                                {Name: "phone", Select: null},
                                {Name: "password", Select: null},
                                {Name: "balance", Select: null},
                                {Name: "idLevel", Select: "Level"},
                                {Name: "preferences", Select: null},
                                {Name: "cardNumber", Select: null},
                                {Name: "namePosition", Select: null},
                                {Name: "percent", Select: null},
                                {Name: "telegram", Select: null},
                                {Name: "dateBegin", Select: null},
                                {Name: "workingShift", Select: "WorkingShift", Join: true}
                            ]
                        })}
                        to={"/Worker"}>Worker</NavLink>
                    <NavLink 
                        onClick={()=> this.setState({
                            Table: "WorkingShift",
                            Form: [
                                {Name: "dateBegin", Select: null},
                                {Name: "dateEnd", Select: null},
                                {Name: "money", Select: null},
                                {Name: "percent", Select: null},
                                {Name: "idRoom", Select: "Room"}
                            ]
                        })}
                        to={"/WorkingShift"}>WorkingShift</NavLink>
                        </div>
                <Tools Table={this.state.Table} Form={this.state.Form}/>
            </div>
	   )
    }
}











